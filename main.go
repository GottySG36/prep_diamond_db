// Utility to merge multiple sequence files into one adn tag them

package main

import (
    "bufio"
    "flag"
    "io"
    "log"
    "os"
    "path"
    "strings"
)

var inf  = flag.String("i", "", "Sequence files to merge, comma seperated")
var out  = flag.String("o", "", "Output file name")
var sep  = flag.String("s", "|", "Seperator used to delimit individual header fields.")
var pID  = flag.String("patient", "", "Patient ID to use in the sequence header tag (will try to guess if empty)")
var samp = flag.String("samples", "",
    `Sample names to use in the sequence header tag, comma seperated.
Order should match input files, otherwise there will be mismatches.
(will try to guess if empty)`,
)

func main() {
    flag.Parse()
    files := strings.Split(*inf, ",")
    patient := *pID
    if patient == "" {
        patient = strings.Split(strings.Split(files[0], "results/")[1], "/")[0]
    }
    samples := make([]string, 0)
    if *samp != "" {
        samples = strings.Split(*samp, ",")
    } else {
        for _, file := range files {
            samples = append(samples, strings.Split(strings.Split(file, "results/")[1], "/")[1])
        }
    }

    o, err := os.OpenFile(*out, os.O_CREATE|os.O_WRONLY, 0644)
    defer o.Close()
    if err != nil {
        log.Fatalf("Error -:- Creating output for writing : %v\n", err)
    }

    w := bufio.NewWriter(o)
    var b strings.Builder

    for i, file := range files {
        taxID := strings.TrimSuffix(path.Base(file), path.Ext(file))
        seq, err := os.Open(file)
        defer seq.Close()
        if err != nil {
            log.Fatalf("Error -:- Opening input : %v\n", err)
        }

        s := bufio.NewReader(seq)
        for {
            lr, more, err := s.ReadLine()
            if err == io.EOF {
                break
            } else if len(lr) == 0 {
                continue
            } else {
                if lr[0] == '>' {
                    b.WriteString(">")
                    b.WriteString(patient)
                    b.WriteString(*sep)
                    b.WriteString(samples[i])
                    b.WriteString(*sep)
                    b.WriteString(taxID)
                    b.WriteString(*sep)
                    b.Write(lr[1:])
                    b.WriteString("\n")
                    w.WriteString(b.String())
                    b.Reset()
                } else if more {
                    b.Write(lr)
                } else {
                    b.Write(lr)
                    b.WriteString("\n")
                    w.WriteString(b.String())
                    b.Reset()
                }
            }
        }
        w.Flush()
    }
}

